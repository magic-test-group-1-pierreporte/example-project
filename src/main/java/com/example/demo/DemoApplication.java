package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {

    /**
     * This is our awesome home endpoint.
     * @return an awesome feature
     */
    @GetMapping("/")
    String home() {
        return "Spring is here!";
    }

    /**
     * Here everything begins.
     * @param args
     */
    public static void main(final String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
